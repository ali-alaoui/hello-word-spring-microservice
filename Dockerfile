FROM openjdk:8
ADD target/helloword.jar helloword.jar
ENTRYPOINT ["java","-jar","helloword.jar"]
EXPOSE 8181